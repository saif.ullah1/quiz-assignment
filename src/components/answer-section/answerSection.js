import './answerSection.css';

const Answer = ({ question, onClick }) => {
    return (
        <div className="answer-section">
              {question.answerOptions?.map((answerOptions,i) => (
                <button className="btn"
                onClick={(e) =>
                onClick(e,answerOptions.isCorrect)
                }
                >
                {answerOptions.answerText}
        </button>
              ))}
            </div>
    )
}

export default Answer;