

const Question = ({ question, currentQuestion }) => {
    return (
        <div className="question-section">
              <div className="question-count">
                <span>Question {currentQuestion+1}</span>
                
              </div>
              <div className="question-text">
                {question[currentQuestion]?.questionText}
              </div>
            </div>
    )
}

export default Question;