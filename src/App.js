import "./App.css";

import React, { useEffect, useState } from "react";
import Question from "./components/question-section/questionSection";
import Answer from "./components/answer-section/answerSection";
import { getQuestionData } from "./Services/question.service";


function App() {
  const [questions,setQuestion]=useState([]);
  const getQuestion = async () => {
    const data = await getQuestionData();
    setQuestion(data);
  }

  useEffect(() => {
    getQuestion();
  }, []);

  // const questions = [
  //   {
  //     questionText: "Who is Prime Minister of India?",
  //     answerOptions: [
  //       { answerText: "Vijay Rupani", isCorrect: false },
  //       { answerText: "Manmohan singh", isCorrect: false },
  //       { answerText: "Narendra Modi", isCorrect: true },
  //       { answerText: "Deep Patel", isCorrect: false }
  //     ]
  //   },
  //   {
  //     questionText: "Who is CEO of Tata?",
  //     answerOptions: [
  //       { answerText: "Jeff Bezos", isCorrect: false },
  //       { answerText: "Ratan Tata", isCorrect: true },
  //       { answerText: "Mukesh Ambani", isCorrect: false },
  //       { answerText: "Gautam Adani", isCorrect: false }
  //     ]
  //   },
  //   {
  //     questionText: "who is richest person in the world?",
  //     answerOptions: [
  //       { answerText: "Jeff Bezos", isCorrect: false },
  //       { answerText: "Elon Musk", isCorrect: true },
  //       { answerText: "Mukesh Ambani", isCorrect: false },
  //       { answerText: "Warren Buffett", isCorrect: false }
  //     ]
  //   },
  //   {
  //     questionText: "how many countries in the world?",
  //     answerOptions: [
  //       { answerText: "120", isCorrect: false },
  //       { answerText: "183", isCorrect: false },
  //       { answerText: "170", isCorrect: false },
  //       { answerText: "195", isCorrect: true }
  //     ]
  //   }
  // ];

  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [showScore, setShowScore] = useState(false);
  const [score, setScore] = useState(0);
  const handleAnswerButtonClick = (e, isCorrect) => {
    if (isCorrect === true) {
      e.target.style.color = 'green'
      setScore(score + 1);
    }
    else {
      e.target.style.color = 'red'
    }

    const nextQuetions = currentQuestion + 1;
    if (nextQuetions < questions.length) {
      setTimeout(() => {
        e.target.style.color = 'white';
        setCurrentQuestion(nextQuetions)
      }
        , 3000)
    } else {
      setShowScore(true);
    }
  };
  return (
    <main>
      <h1 className="heading">Quiz</h1>
      <div className="app">
        {showScore ? (
          <div className="score-section">
            You scored {score} out of {questions.length}
          </div>
        ) : (
          questions.length > 0 ?
          <>
            <Question question = {questions} currentQuestion = {currentQuestion}/>
            <Answer question ={questions[currentQuestion]} onClick = {handleAnswerButtonClick}/>
          </>: <p>Fetching question </p>
        )}
      </div>
    </main>
  );
}

export default App;